//
//  ImageViewController.swift
//  TableViewsCocoapods
//
//  Created by Roger on 8/1/18.
//  Copyright © 2018 Roger. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {

    var pokemon:Pokemon?
    
    
    @IBOutlet weak var propiedad1: UILabel!
    @IBOutlet weak var propiedad2: UILabel!
    
    @IBOutlet weak var propiedad3: UILabel!
    @IBOutlet weak var imageOutlet: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = pokemon?.pkName
        propiedad1.text = pokemon?.pkTipo
        propiedad2.text = pokemon?.pkHabilidad
        propiedad3.text = pokemon?.pkMovimeinto

        
        let pkService = PokemonService()
        pkService.getPokemonImage(id: (pokemon?.pkId)!) { (pkImage) in
            self.imageOutlet.image = pkImage
        }
        
    }



    
}
