//
//  PokemonTableViewCell.swift
//  TableViewsCocoapods
//
//  Created by Roger on 5/1/18.
//  Copyright © 2018 Roger. All rights reserved.
//

import UIKit

class PokemonTableViewCell: UITableViewCell {

    
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func fillData(pokemon:Pokemon){
        idLabel.text = "\(pokemon.pkId ?? 00)"
        nameLabel.text = pokemon.pkName
    }

}
