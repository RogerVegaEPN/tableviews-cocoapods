//
//  Abilities.swift
//  TableViewsCocoapods
//
//  Created by Roger on 8/1/18.
//  Copyright © 2018 Roger. All rights reserved.
//

import Foundation
import ObjectMapper

class Abilitites: Mappable {
    var ability:[Ability]?
    
    required init?(map:Map){
        
    }
    
    func mapping(map: Map) {
        ability <- map["ability"]
    }
}
