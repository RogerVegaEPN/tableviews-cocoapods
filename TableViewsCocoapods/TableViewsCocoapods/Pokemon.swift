//
//  Pokemon.swift
//  TableViewsCocoapods
//
//  Created by Roger on 27/11/17.
//  Copyright © 2017 Roger. All rights reserved.
//

import Foundation
import ObjectMapper

class Pokemon: Mappable {
    
    var pkId:Int?
    var pkName:String?
    var pkWeight:Double?
    var pkHeight:Double?
    var pkVersion:String?
    var pkHabilidad:String?
    var pkMovimeinto:String?
    var pkTipo:String?
    
    required init?(map:Map){
        
    }
    
    func mapping(map: Map) {
        pkId <- map["id"]
        pkName <- map["name"]
        pkWeight <- map["weight"]
        pkHeight <- map["height"]
        pkVersion <- map["game_indices.0.version.name"]
        pkHabilidad <- map["abilities.0.ability.name"]
        pkMovimeinto <- map["moves.0.move.name"]
        pkTipo <- map["types.0.type.name"]
    }
}


