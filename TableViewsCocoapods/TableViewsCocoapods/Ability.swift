//
//  Ability.swift
//  TableViewsCocoapods
//
//  Created by Roger on 8/1/18.
//  Copyright © 2018 Roger. All rights reserved.
//

import Foundation
import ObjectMapper

class Ability: Mappable {

    
    var name:String?
    
    required init?(map:Map){
        
    }
    func mapping(map: Map) {
        name <- map["name"]
    }
}
