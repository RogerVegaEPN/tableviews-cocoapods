//
//  PockemonService.swift
//  TableViewsCocoapods
//
//  Created by Roger on 5/1/18.
//  Copyright © 2018 Roger. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import AlamofireImage


protocol PokemonServiceDelegate{
    func first20Pokemon(pokemon:[Pokemon])
}

class PokemonService{
    var delegate:PokemonServiceDelegate?
 
    func get20FirstPokemon(){
        
        var pokemonArray:[Pokemon] = []
        let group = DispatchGroup()
        for i in 1...20{
            
            group.enter()
            Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(i)").responseObject { (response: DataResponse<Pokemon>) in
                let pokemon = response.result.value
                pokemonArray.append(pokemon!)
                group.leave()
            }
        }
        
        group.notify(queue: .main){
            pokemonArray.sort(by: { (pk1, pk2) -> Bool in
                pk1.pkId! < pk2.pkId!
            })
            
            self.delegate?.first20Pokemon(pokemon: pokemonArray)
        }
        

        
        
    }
    
    func getPokemonImage(id:Int , completion:@escaping (UIImage)->()) {
        let url="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(id).png"
        Alamofire.request(url).responseImage { response in
            debugPrint(response)
            
            if let image = response.result.value {
                print("image downloaded: \(image)")
                completion(image)
            }
        }
    }
}
