//
//  TableViewViewController.swift
//  TableViewsCocoapods
//
//  Created by Roger on 5/1/18.
//  Copyright © 2018 Roger. All rights reserved.
//

import UIKit

class TableViewViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, PokemonServiceDelegate {
    
    
    @IBOutlet weak var tableViewOutlet: UITableView!
    
    let pokemonService = PokemonService()
    var pokemonArray:[Pokemon] = []
    var selectedPokemon = 0
    
    func first20Pokemon(pokemon: [Pokemon]) {
        pokemonArray = pokemon
        tableViewOutlet.reloadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        pokemonService.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        pokemonService.get20FirstPokemon()
    }

    //Secciones
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //Filas por seccion
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return pokemonArray.count
        default:
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pokeCelda") as! PokemonTableViewCell
        //cell.textLabel?.text = "hola"
        //cell.textLabel?.text = "\(indexPath)"
        //cell.textLabel?.text = pokemonArray[indexPath.row].pkName
        //cell.textLabel?.text = pokemonArray[indexPath.row].pkName
        cell.fillData(pokemon: pokemonArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        selectedPokemon = indexPath.row
        return indexPath
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! ImageViewController
        destination.pokemon = pokemonArray[selectedPokemon]
    }
    
}
